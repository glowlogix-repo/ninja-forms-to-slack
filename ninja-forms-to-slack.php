<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.glowlogix.com
 * @since             1.0.0
 * @package           Ninja_Forms_To_Slack
 *
 * @wordpress-plugin
 * Plugin Name:       Ninja Forms To Slack
 * Plugin URI:        http://www.glowlogix.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Muhammad Usama
 * Author URI:        http://www.glowlogix.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ninja-forms-to-slack
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ninja-forms-to-slack-activator.php
 */
function activate_ninja_forms_to_slack() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ninja-forms-to-slack-activator.php';
	Ninja_Forms_To_Slack_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ninja-forms-to-slack-deactivator.php
 */
function deactivate_ninja_forms_to_slack() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ninja-forms-to-slack-deactivator.php';
	Ninja_Forms_To_Slack_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ninja_forms_to_slack' );
register_deactivation_hook( __FILE__, 'deactivate_ninja_forms_to_slack' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ninja-forms-to-slack.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ninja_forms_to_slack() {

	$plugin = new Ninja_Forms_To_Slack();
	$plugin->run();

}
run_ninja_forms_to_slack();
