<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.glowlogix.com
 * @since      1.0.0
 *
 * @package    Ninja_Forms_To_Slack
 * @subpackage Ninja_Forms_To_Slack/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ninja_Forms_To_Slack
 * @subpackage Ninja_Forms_To_Slack/includes
 * @author     Muhammad Usama <m.usama.masood@gmail.com>
 */
class Ninja_Forms_To_Slack_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
