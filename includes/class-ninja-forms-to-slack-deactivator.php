<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.glowlogix.com
 * @since      1.0.0
 *
 * @package    Ninja_Forms_To_Slack
 * @subpackage Ninja_Forms_To_Slack/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ninja_Forms_To_Slack
 * @subpackage Ninja_Forms_To_Slack/includes
 * @author     Muhammad Usama <m.usama.masood@gmail.com>
 */
class Ninja_Forms_To_Slack_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
