<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.glowlogix.com
 * @since      1.0.0
 *
 * @package    Ninja_Forms_To_Slack
 * @subpackage Ninja_Forms_To_Slack/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ninja_Forms_To_Slack
 * @subpackage Ninja_Forms_To_Slack/includes
 * @author     Muhammad Usama <m.usama.masood@gmail.com>
 */
class Ninja_Forms_To_Slack_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ninja-forms-to-slack',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
