<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.glowlogix.com
 * @since      1.0.0
 *
 * @package    Ninja_Forms_To_Slack
 * @subpackage Ninja_Forms_To_Slack/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
